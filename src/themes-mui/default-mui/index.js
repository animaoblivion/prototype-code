import React from 'react';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';

import 'typeface-roboto';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css'

import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';

const primary = blue[500]; // #F44336

// All the following keys are optional.
// We try our best to provide a great default value.
const theme = createMuiTheme({
  palette: {
    primary: {
      light: blue[300],
      main: "#007837",
      dark: "#125430",
      contrastText: '#fff',
    },
    secondary: {
      light: pink[300],
      main: pink[500],
      dark: pink[700],
      contrastText: '#fff',
    },
    error: {
      main: '#C20E1A'
    },
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.1,
  },
});

const Theme = props => {
  return <ThemeProvider theme={theme}>
    {props.children}
  </ThemeProvider>
}

export default Theme;
