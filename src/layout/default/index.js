import React from 'react';
import Container from '@material-ui/core/Container';

import Imports from './imports';
import Header from './header';
import Footer from './footer';

import appConf from '../../components/app-conf';
const Theme = appConf[process.env.REACT_APP_NAME].theme;

const thqLanding = props => {
  return <Theme.Main>
    <Container maxWidth="md">
      <Imports {...props} />
    </Container>
  </Theme.Main>
}

export default thqLanding;
