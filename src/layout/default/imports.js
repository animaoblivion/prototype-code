import React from 'react';

import Modules from '../../modules/default';

const thqLandingImports = props => {
  return Modules.map((Module, i) => {
    return <Module.Routes {...props} key={i} />
  });
}

export default thqLandingImports;
