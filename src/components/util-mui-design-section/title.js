import React from 'react';

import Typography from '@material-ui/core/Typography';

const Title = props => {
  return <Typography variant="h4" gutterBottom>
    { props.children }
  </Typography>
}

export default Title;
