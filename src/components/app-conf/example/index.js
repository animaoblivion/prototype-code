import setup from './setup';
import modules from './modules';
import assets from './assets';
import theme from './theme';

export default {
  setup,
  modules,
  assets,
  theme
}
