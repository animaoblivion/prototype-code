import React from 'react';

import { FormFeedback } from 'reactstrap';

const Invalid = props => {
  const { errors } = props;
  if (!errors) return null;
  const messages = errors.map((error, index) => {
    return <p className="title-regular color-danger" key={index}>{error}</p>
  });
  return <FormFeedback className="space-standard-top space-standard-bottom">
    { messages }
  </FormFeedback>
}

export default Invalid;
