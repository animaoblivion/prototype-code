import React from 'react';

import { FormFeedback } from 'reactstrap';

const Valid = props => {
  const { errors } = props;
  if (!errors) return null;
  const messages = errors.map((error, index) => {
    return <p key={index}>{error}</p>
  });
  return <FormFeedback valid>
    { messages }
  </FormFeedback>
}

export default Valid;
