import appConf from "../app-conf";
const setup = appConf[process.env.REACT_APP_NAME];
const url = process.env.HOST;
const environment = process.env.NODE_ENV;
const project = "Cart";
const server = null;
const service = null;
const config = {
  details: {
    project,
    server,
    environment,
    service,
  },
  application: {
    products: {
      api: {
        headers: {
          'Content-Type': 'application/json'
        },
        endpoints: {
          list: {
            url: url + '/account/admin/login',
            method: 'GET'
          }
        }
      },
    }
  },
  routes: {
    label: "Product Detail",
    prefix: "/product/:name"
  },
  setup
}

export default config;
