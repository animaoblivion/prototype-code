import { createContext } from 'react';
import { decorate, observable, computed } from 'mobx';

export class Store {

  loading = true;
  data = false;
  status = false;

  setLoading = (data) => {
    this.loading = data;
  }

  setData = (data) => {
    this.data = data;
  }

  setStatus = (status) => {
    this.status = status;
  }

  isData = () => {
    return (this.status && !this.loading) ? this.data : false;
  }

}

const store = new Store();

decorate(store, {
  loading: observable,
  data: observable,
  status: observable
  /*
  todos: observable,
  remainingTodos: computed
  */
})

export default createContext(store);
