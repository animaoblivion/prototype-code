import React from 'react'

/* modules */
import ProductDetail from '../detail';
/* modules */

const index = props => {
  const { products, handleProductAdd } = props.data;
  return products.map((product, index) => {
    const product_data = { product, handleProductAdd }
    return <ProductDetail key={index} data={product_data} />
  });

}

export default index;
