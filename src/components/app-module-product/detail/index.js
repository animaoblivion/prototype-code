import React from 'react'

/* mui */
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
/* mui */

/* packages */
import Box from '../../util-box';
import Listing from "../../util-mui-list";
import Skeleton from "../../util-skeleton";
import Fetch from '../../util-fetch';
import Alert from '../../util-alert';
import LocalStorage from '../../util-local-storage';
import Inputs from '../../util-inputs';
import Button from '../../util-mui-buttons';
import Input from '../../util-mui-inputs';
import useForm from '../../util-hook-useform';
import useSuggest from '../../util-hook-usesuggest';
import validation from '../../util-inputs-validation';
import Breadcrumbs from '../../util-mui-breadcrumbs';
/* packages */

/* mui */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    marginBottom: 10,
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));
/* mui */

const index = props => {

  /* mui */
  const classes = useStyles();
  /* mui */

  const { product, handleProductAdd } = props.data;

  return <Paper className={classes.paper}>
    <Typography variant="h6"><b>{product.name}</b></Typography>
    <Typography variant="body2" gutterBottom><small>PRICE</small> <b>{product.price.toFixed(2)}</b></Typography>
    { handleProductAdd &&
      <Button.PrimarySmall onClick={handleProductAdd.bind(this, product)}>Add to cart</Button.PrimarySmall>
    }
  </Paper>

}

export default index;
