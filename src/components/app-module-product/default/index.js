import React, { useEffect, useContext, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import { observer, useObserver } from 'mobx-react-lite'

/* mui */
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
/* mui */

/* packages */
import Box from '../../util-box';
import Listing from "../../util-mui-list";
import Skeleton from "../../util-skeleton";
import Fetch from '../../util-fetch';
import Alert from '../../util-alert';
import LocalStorage from '../../util-local-storage';
import Inputs from '../../util-inputs';
import Button from '../../util-mui-buttons';
import Input from '../../util-mui-inputs';
import useForm from '../../util-hook-useform';
import useSuggest from '../../util-hook-usesuggest';
import validation from '../../util-inputs-validation';
import Breadcrumbs from '../../util-mui-breadcrumbs';
/* packages */

import CONFIG from '../config';

/* mui */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(3),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));
/* mui */

const index = observer((props) => {

  /* mui */
  const classes = useStyles();
  /* mui */

  const config = CONFIG.application.products;

  return <Paper className={classes.paper}>
    example
  </Paper>

})

export default index;
