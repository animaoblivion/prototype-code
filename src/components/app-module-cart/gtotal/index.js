import React from 'react'

/* mui */
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
/* mui */

/* packages */
import Box from '../../util-box';
import Listing from "../../util-mui-list";
import Skeleton from "../../util-skeleton";
import Fetch from '../../util-fetch';
import Alert from '../../util-alert';
import LocalStorage from '../../util-local-storage';
import Inputs from '../../util-inputs';
import Button from '../../util-mui-buttons';
import Input from '../../util-mui-inputs';
import useForm from '../../util-hook-useform';
import useSuggest from '../../util-hook-usesuggest';
import validation from '../../util-inputs-validation';
import Breadcrumbs from '../../util-mui-breadcrumbs';
/* packages */

/* mui */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    marginBottom: 10,
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));
/* mui */

const index = props => {

  /* mui */
  const classes = useStyles();
  /* mui */

  const { carts } = props.data;

  const total = Object.keys(carts).map((product, index) => {
    return [...carts[product]].reduce((acc, obj) => { return acc + obj.price; }, 0);
  });
  const gtotal = [...total].reduce((acc, obj) => { return acc + obj; }, 0);

  return <Paper className={classes.paper}>
    <Typography variant="h6"><small>OVERALL TOTAL</small></Typography>
    <Typography variant="h6"><b>{gtotal.toFixed(2)}</b></Typography>
  </Paper>

}

export default index;
