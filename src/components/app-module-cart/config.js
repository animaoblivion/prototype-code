import appConf from "../app-conf";
console.log(process.env);
const setup = appConf[process.env.REACT_APP_NAME];
const environment = process.env.NODE_ENV;
const project = "Cart";
const server = null;
const service = null;
const config = {
  details: {
    project,
    server,
    environment,
    service,
  },
  application: {
    login: {
      api: {
        headers: {
          'Content-Type': 'application/json'
        },
        endpoints: {
          login: {
            url: setup.setup.url + '/account/admin/login',
            method: 'POST'
          }
        }
      },
      objects: {
        cardnumber: { name: "cardnumber", initial: "", type: "text", label: "Card Number", helpText: false, rules: ["isNotNull"] },
        pin: { name: "pin", initial: "", type: "password", label: "Pin Code", helpText: false, rules: false }
      },
      responses: {
        "fail": "Incorrect email or password"
      }
    }
  },
  routes: {
    label: "Cart",
    prefix: "/cart"
  },
  setup
}

export default config;
