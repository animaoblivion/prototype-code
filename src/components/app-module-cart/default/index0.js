import React, { useEffect, useContext, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import { observer, useObserver } from 'mobx-react-lite'

/* mui */
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
/* mui */

/* packages */
import Box from '../../util-box';
import Listing from "../../util-mui-list";
import Skeleton from "../../util-skeleton";
import Fetch from '../../util-fetch';
import Alert from '../../util-alert';
import LocalStorage from '../../util-local-storage';
import Inputs from '../../util-inputs';
import Button from '../../util-mui-buttons';
import Input from '../../util-mui-inputs';
import useForm from '../../util-hook-useform';
import useSuggest from '../../util-hook-usesuggest';
import validation from '../../util-inputs-validation';
import Stores from '../../app-plugin-stores';
import Breadcrumbs from '../../util-mui-breadcrumbs';
/* packages */

import CONFIG from '../config';

/* mui */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(3),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));
/* mui */

const index = observer((props) => {

  /* mui */
  const classes = useStyles();
  /* mui */

  const config = CONFIG.application.login.objects;

  const login = () => {
    const { history } = props;
    const { auth_landing_module } = CONFIG.setup.setup;
    history.push(auth_landing_module);
  }

  const initials = (config) => {
    let values = {};
    for (const key in config) { values[key] = config[key].initial; }
    return values;
  }

  const {
    values,
    errors,
    handleChange,
    handleSubmit,
  } = useForm(login, validation, config, initials(config));

  const cardnumber = { ...config.cardnumber, errors: errors.cardnumber, value: values.cardnumber, handleChange };
  const pin = { ...config.pin, errors: errors.pin, value: values.pin, handleChange };

  const logo = CONFIG.setup.assets.logo;

  return <div className={classes.root}>
    <Grid container spacing={1}>
      <Grid item xs>
        <Typography variant="h4" align="center" gutterBottom>
          <img src={logo} />
        </Typography>
        <Paper className={classes.paper}>
          <Typography variant="h4" align="center" gutterBottom>
            <b>Sign in</b>
          </Typography>
          <form onSubmit={handleSubmit} noValidate>
            <Input.Text {...cardnumber} />
            <Input.Password {...pin} />
            <section className="space-standard-top">
            <Button.NormalBlock>Sign in</Button.NormalBlock>
            </section>
          </form>
          <br/>
          <Grid container spacing={0}>
            <Grid item xs>
              <Typography variant="body2" gutterBottom>
                <Link to="/u/pin-reset">Forgot your pin code?</Link>
              </Typography>
            </Grid>
            <Grid item xs>
              <Typography variant="body2" gutterBottom>
                <Link to="/u/signup">Dont have an account? Sign up here.</Link>
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  </div>

})

export default index;
