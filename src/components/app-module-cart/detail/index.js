import React from 'react'

/* mui */
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
/* mui */

/* packages */
import Box from '../../util-box';
import Listing from "../../util-mui-list";
import Skeleton from "../../util-skeleton";
import Fetch from '../../util-fetch';
import Alert from '../../util-alert';
import LocalStorage from '../../util-local-storage';
import Inputs from '../../util-inputs';
import Button from '../../util-mui-buttons';
import Input from '../../util-mui-inputs';
import useForm from '../../util-hook-useform';
import useSuggest from '../../util-hook-usesuggest';
import validation from '../../util-inputs-validation';
import Breadcrumbs from '../../util-mui-breadcrumbs';
/* packages */

/* mui */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    marginBottom: 10,
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));
/* mui */

const index = props => {

  /* mui */
  const classes = useStyles();
  /* mui */

  const { product, handleProductRemove, items } = props.data;

  const price = items[0].price;
  const quantity = items.length;
  const total = [...items].reduce((acc, obj) => { return acc + obj.price; }, 0);

  return <Paper className={classes.paper}>
    <Typography variant="h6"><b>{product}</b></Typography>
    <Typography variant="body2"><small>PRICE</small> <b>$ {price.toFixed(2)}</b></Typography>
    <Typography variant="body2"><small>QUANTITY</small> <b>x {quantity}</b></Typography>
    <Typography variant="body2" gutterBottom><small>TOTAL</small> <b>$ {total.toFixed(2)}</b></Typography>
    { handleProductRemove &&
      <Button.PrimarySmall onClick={handleProductRemove.bind(this, product)}>Remove</Button.PrimarySmall>
    }
  </Paper>

}

export default index;
