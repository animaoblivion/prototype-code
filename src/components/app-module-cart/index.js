import React from 'react';

import Routes from './routes';
import Links from './links';

export default {
  Routes,
  Links
};
