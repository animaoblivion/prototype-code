import React from 'react'

/* modules */
import CartDetail from '../detail';
/* modules */

const index = props => {
  const { carts, handleProductRemove } = props.data;
  return Object.keys(carts).map((product, index) => {
    const cart_data = { product, handleProductRemove, items: carts[product] }
    return <CartDetail key={index} data={cart_data} />
  });
}

export default index;
