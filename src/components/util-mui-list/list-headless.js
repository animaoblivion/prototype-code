import React from "react"
import PropTypes from 'prop-types';

/* style */
import './styles.css';
/* reactstrap */
import { Row, Col } from 'reactstrap';
/* local */

const Detail = (props) => {
  const { config, data } = props || [];
  let row = [];
  data.map((dd,di) => {
    let column = [];
    config.map((cd,ci) => {
      let template = cd.template;
      let content = dd[cd.field];
      if (template) content = template(content, dd);
      column.push(<Col key={ci}>{content}</Col>);

    })
    row.push(<Row className="atom-el-list-detail" key={di}>{column}</Row>);
  });
  return row;
}

const Index = (props) => {

  const detail = Detail(props);

  return <Row className="atom-el-list">
    <Col>
      {detail}
    </Col>
  </Row>
}

export default Index;
