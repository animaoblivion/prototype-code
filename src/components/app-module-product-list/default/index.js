import React, { useEffect, useContext, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import { observer, useObserver } from 'mobx-react-lite'

/* mui */
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
/* mui */

/* packages */
import Box from '../../util-box';
import Listing from "../../util-mui-list";
import Skeleton from "../../util-skeleton";
import Fetch from '../../util-fetch';
import Alert from '../../util-alert';
import LocalStorage from '../../util-local-storage';
import Inputs from '../../util-inputs';
import Button from '../../util-mui-buttons';
import Input from '../../util-mui-inputs';
import useForm from '../../util-hook-useform';
import useSuggest from '../../util-hook-usesuggest';
import validation from '../../util-inputs-validation';
import Breadcrumbs from '../../util-mui-breadcrumbs';
/* packages */

/* stores */
import ProductListStore from '../../app-module-product/stores/list';
/* stores */

/* modules */
import ProductList from '../../app-module-product/list';
import CartList from '../../app-module-cart/list';
import CartTotal from '../../app-module-cart/gtotal';
/* modules */

import CONFIG from '../config';

/* mui */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    marginBottom: 5,
    padding: theme.spacing(5),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));
/* mui */

const index = observer((props) => {

  /* mui */
  const classes = useStyles();
  /* mui */

  const config = CONFIG.application.products;

  /* LS */
  const getLS = () => {
    const LS = LocalStorage.get();
    return LS.cart ? JSON.parse(LS.cart) : {};
  };

  const addLS = (cart) => {
    LocalStorage.removeAll();
    return LocalStorage.add('cart', JSON.stringify(cart));
  }
  /* LS */

  /* states: list */
  const productContext  = useContext(ProductListStore);
  const loadProduct = async () => {
    productContext.setLoading(true);
    const contextFetch = await Fetch({
      url: CONFIG.application.products.api.endpoints.list.url,
      config: {
        method: CONFIG.application.products.api.endpoints.list.method,
        headers: CONFIG.application.products.api.headers
      }
    });
    productContext.setStatus(contextFetch[0]);
    productContext.setData(contextFetch[1]);
    productContext.setLoading(false);
  }

  useEffect(() => {
    loadProduct();
  }, []);

  const products = productContext.isData() ? productContext.isData().data : false;
  /* states: list */

  /* states: cart */
  const [cart, setCart] = useState(getLS());
  const handleProductAdd = (data) => {
    const { name, price } = data;
    const keys = Object.keys(cart);
    let index = keys.indexOf(name) < 0 ? false : true;
    if (index) {
      cart[name].push(data);
      setCart({...cart});
    } else {
      setCart({ ...cart, [name]: [data] });
    }
  }

  const handleProductRemove = (product) => {
    const length = cart[product].length > 1 ? true : false;
    if (length) cart[product].pop();
    if (!length) delete cart[product];
    setCart({...cart});
  }

  useEffect(() => {
    addLS(cart);
  }, [cart]);
  /* states: cart */

  const product_data = { products, handleProductAdd }
  const cart_data = { carts: cart, handleProductRemove }
  const isCart = Object.keys(cart).length < 1 ? false : true;
  const cartLength = Object.keys(cart).length;

  if (!products) return null;

  return <div className={classes.root}>
    <Grid container spacing={5}>

      <Grid item sm>
        <Typography variant="h6"><b>PRODUCTS</b></Typography>
        <ProductList data={product_data} />
      </Grid>

      <Grid item sm>
        { !isCart &&
          <div>
          <Typography variant="h6"><b>&nbsp;</b></Typography>
          <Paper className={classes.paper}>
            <Typography variant="body2" gutterBottom><b>Hi there!</b></Typography>
            <Typography variant="body2" gutterBottom><b>You may want to add item in your cart.</b></Typography>
          </Paper>
          </div>
        }
        { isCart &&
          <div>
            <Typography variant="h6"><b>&nbsp;</b></Typography>
            <Paper className={classes.paper}>
            <Typography variant="body2" gutterBottom><b>Hi there!</b></Typography>
            <Typography variant="body2" gutterBottom><b>You have {cartLength} product ready in your cart.</b></Typography>
            <CartList data={cart_data} />
            <CartTotal data={cart_data} />
            </Paper>
          </div>
        }
      </Grid>

    </Grid>
  </div>

  // return <div>
  //   <ProductList data={product_data} />
  // </div>

})

export default index;
