import appConf from "../app-conf";
const setup = appConf[process.env.REACT_APP_NAME];
const url = process.env.HOST;
const environment = process.env.NODE_ENV;
const project = "Cart";
const server = null;
const service = null;
const config = {
  details: {
    project,
    server,
    environment,
    service,
  },
  application: {
    products: {
      api: {
        headers: {
          'Content-Type': 'application/json'
        },
        endpoints: {
          list: {
            url: "http://localhost:3000/data/product.json",
            method: 'GET'
          }
        }
      },
    }
  },
  routes: {
    label: "Product List",
    prefix: "/"
  },
  setup
}

export default config;
