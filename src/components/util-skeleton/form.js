import React from "react"

import './style.css';

const Atom = props => {

  return <div className="skeleton-container">
    <div className="line xl h15"></div>
    <div className="line md h15"></div>
    <div className="line xl h40"></div>
  </div>

}

export default Atom;
