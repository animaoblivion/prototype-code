import React from "react"

import { Container, Row, Col } from 'reactstrap';

import './style.css';

const Listing = props => {

  return <Row>
    <Col sm="12">
      <div className="skeleton-container">
        <div className="line sm h60"></div>
        <div className="line sm h60"></div>
        <div className="line sm h60"></div>
      </div>
    </Col>
    <Col sm="12">
      <div className="skeleton-container">
        <div className="line sm h15"></div>
        <div className="line sm h15"></div>
        <div className="line sm h15"></div>
      </div>
    </Col>
    <Col sm="12">
      <div className="skeleton-container">
        <div className="line sm h30"></div>
        <div className="line sm h30"></div>
        <div className="line sm h30"></div>
      </div>
    </Col>
    <Col sm="12">
      <div className="skeleton-container">
        <div className="line sm h30"></div>
        <div className="line sm h30"></div>
        <div className="line sm h30"></div>
      </div>
    </Col>
  </Row>
}

export default Listing;
