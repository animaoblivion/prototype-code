import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

const Invalid = props => {
  const { errors } = props;
  if (!errors) return null;
  return errors.map((error, index) => {
    return <FormHelperText error key={index}>
      { error }
    </FormHelperText>
  });
}

export default Invalid;
