import React, { useEffect, useRef, useState } from 'react'

import './style.css';

import Autosuggest from 'react-autosuggest';
import { FormGroup, Label, Input } from 'reactstrap';

import Feeds from '../util-inputs-feedback';

const Text = props => {

  const { name, initial, type, label, errors, value, handleChange, suggestionProps, isSelected, onResetSelected, selectedValue, displayTemplate, removeText } = props;

  const theme_valid = {
    container:                'space-standard-top space-standard-bottom',
    containerOpen:            'space-standard-top space-standard-bottom',
    input:                    'form-control',
    inputOpen:                'react-autosuggest__input--open',
    inputFocused:             'react-autosuggest__input--focused',
    suggestionsContainer:     'react-autosuggest__suggestions-container',
    suggestionsContainerOpen: 'react-autosuggest__suggestions-container--open',
    suggestionsList:          'react-autosuggest__suggestions-list',
    suggestion:               'react-autosuggest__suggestion',
    suggestionFirst:          'react-autosuggest__suggestion--first',
    suggestionHighlighted:    'react-autosuggest__suggestion--highlighted',
    sectionContainer:         'react-autosuggest__section-container',
    sectionContainerFirst:    'react-autosuggest__section-container--first',
    sectionTitle:             'react-autosuggest__section-title'
  }
  const theme_invalid = {
    container:                'space-standard-top space-standard-bottom',
    containerOpen:            'space-standard-top space-standard-bottom',
    input:                    'is-invalid form-control',
    inputOpen:                'react-autosuggest__input--open',
    inputFocused:             'react-autosuggest__input--focused',
    suggestionsContainer:     'react-autosuggest__suggestions-container',
    suggestionsContainerOpen: 'react-autosuggest__suggestions-container--open',
    suggestionsList:          'react-autosuggest__suggestions-list',
    suggestion:               'react-autosuggest__suggestion',
    suggestionFirst:          'react-autosuggest__suggestion--first',
    suggestionHighlighted:    'react-autosuggest__suggestion--highlighted',
    sectionContainer:         'react-autosuggest__section-container',
    sectionContainerFirst:    'react-autosuggest__section-container--first',
    sectionTitle:             'react-autosuggest__section-title'
  }

  const theme = !errors ? theme_valid : theme_invalid;

  let selectedDisplay = null;
  if (selectedValue) {
    const { suggestion } = JSON.parse(selectedValue);
    selectedDisplay = displayTemplate(suggestion);
  }

  return <FormGroup className="space-semi-top space-semi-bottom">
    { !isSelected && <Label for={name}>{label}</Label> }
    { !isSelected && <Autosuggest theme={theme} {...suggestionProps} /> }
    { isSelected && <div onClick={onResetSelected}>
      {selectedDisplay}
      <a href="#" className="color-link">{removeText}</a>
    </div> }
    <Input invalid className="" type="hidden" name={name} id={name} />
    <Feeds.Invalid errors={errors} />
  </FormGroup>

}

export default Text;
