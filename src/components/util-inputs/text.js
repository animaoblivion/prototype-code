import React, { useEffect, useRef, useState } from 'react'

import './style.css';

import { FormGroup, Label, Input } from 'reactstrap';

import Feeds from '../util-inputs-feedback';

const Text = props => {
  const { name, initial, type, label, errors, value, handleChange } = props;
  return <FormGroup className="space-semi-top space-semi-bottom">
    <Label for={name}>{label}</Label>
    { !errors && <Input className="" type={type} name={name} id={name} value={value} onChange={handleChange} /> }
    { errors && <Input invalid className="" type={type} name={name} id={name} value={value} onChange={handleChange} /> }
    <Feeds.Invalid errors={errors} />
  </FormGroup>
}

export default Text;
