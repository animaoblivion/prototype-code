import React, { useEffect, useRef, useState } from 'react'

import './style.css';

import { FormGroup, Label, Input } from 'reactstrap';

import Feeds from '../util-inputs-feedback';

const collections = (collection) => {
  return collection.map((data, index) => {
    return <option key={index} value={data.value}>{data.name}</option>
  });
}

const Select = props => {

  const { name, initial, type, label, options, errors, value, handleChange } = props;
  const selectOptions = collections(options);
  return <FormGroup className="space-semi-top space-semi-bottom">
    <Label for={name}>{label}</Label>
    { !errors && <Input className="" type={type} name={name} id={name} onChange={handleChange}>
      <option>Select</option>
      {selectOptions}
      </Input>
    }
    { errors && <Input invalid className="" type={type} name={name} id={name} onChange={handleChange}>
      <option>Select</option>
      {selectOptions}
      </Input>
    }
    <Feeds.Invalid errors={errors} />
  </FormGroup>

}

export default Select;
