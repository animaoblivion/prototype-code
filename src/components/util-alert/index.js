import Success from './alert-success';
import Fail from './alert-fail';
import Information from './alert-information';
import Help from './alert-help';

export default {
  Success,
  Fail,
  Information,
  Help
}
