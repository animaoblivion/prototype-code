import React from "react"

import './style.css';

import { Alert } from 'reactstrap';

const Atom = props => {
  const { children } = props;
  return <Alert color="primary" className="app-information-style">
    {children}
  </Alert>

}

export default Atom;
