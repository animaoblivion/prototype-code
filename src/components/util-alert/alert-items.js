import React from "react"

const Atom = (items) => {
  return items.map((item,i) => {
    return <p key={i} className="mb-0">{item}</p>
  });
}

export default Atom;
