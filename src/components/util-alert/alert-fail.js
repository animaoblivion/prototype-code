import React from "react"

import { Alert } from 'reactstrap';

import Items from './alert-items';

const Atom = (props) => {
  const { children, items } = props;
  if (!items) return null;
  const loop = Items(items);
  return <Alert color="danger" className="app-fail-style">
    <p className="mb-0"><b>{children}</b></p>
    {loop}
  </Alert>
}

export default Atom;
