import React from "react"

/* style */
import './style.css';
/* reactstrap */
import { Alert, Row, Col } from 'reactstrap';

const Loop = (message) => {
  return message.map((m,i) => {
    return <p key={i} className="mb-0"><small>{m}</small></p>
  });
}

const Index = (props) => {
  const { type, message } = props;
  if (!message) return null;
  const css = (type) ? "success" : "danger";
  const loop = Loop(message);
  return <Row className="atom-message mb-0">
    <Col className="mb-0">
      <Alert color={css}>
      {loop}
      </Alert>
    </Col>
  </Row>
}

export default Index;
