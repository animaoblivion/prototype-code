import React from 'react';
import Button from '@material-ui/core/Button';

const NormalBlock = props => {
  const { children, onClick } = props;
  return <Button fullWidth variant="contained" color="primary" type="submit">
    {children}
  </Button>
}

export default NormalBlock;
