import React from 'react';
import Button from '@material-ui/core/Button';

const PrimaryOutlineBlock = props => {
  const { children, onClick } = props;
  const handles = { onClick }
  return <Button fullWidth variant="outlined" color="primary" {...handles} >
    {children}
  </Button>
}

export default PrimaryOutlineBlock;
