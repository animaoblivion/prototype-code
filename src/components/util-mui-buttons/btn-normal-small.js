import React from 'react';
import Button from '@material-ui/core/Button';

const NormalSmall = props => {
  const { children, onClick } = props;
  return <Button variant="contained" color="primary" size="small" type="submit">
    {children}
  </Button>
}

export default NormalSmall;
