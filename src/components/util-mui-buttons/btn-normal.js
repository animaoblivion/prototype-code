import React from 'react';
import Button from '@material-ui/core/Button';

const Primary = props => {
  const { children, onClick } = props;
  return <Button variant="contained" color="primary" type="submit">
    {children}
  </Button>
}

export default Primary;
