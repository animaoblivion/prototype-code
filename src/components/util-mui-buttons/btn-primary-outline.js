import React from 'react';
import Button from '@material-ui/core/Button';

const PrimaryOutline = props => {
  const { children, onClick } = props;
  const handles = { onClick }
  return <Button variant="outlined" color="primary" {...handles} >
    {children}
  </Button>
}

export default PrimaryOutline;
