import Rules from "./rules";

const getRules = (lists, config, field, dataset) => {
  const collection = [];
  for (let i=0;i<lists.length;i++) {
    let validation = Rules[lists[i]];
    let value = dataset[field];
    let data = { value, config, dataset };
    let result = validation(data);
    if (result !== true) collection.push(result);
  }
  return collection;
}

const Atom = (config, dataset) => {
  let errors = {};
  let isValid = true;
  for (const key in dataset) {
    let fieldErrors = null;
    let fieldRules = config[key].rules;
    fieldErrors = getRules(fieldRules, config[key], key, dataset);
    errors[key] = (fieldErrors.length > 0) ? fieldErrors : [];
    if (isValid && fieldErrors.length > 0) isValid = false;
  }
  return { errors, isValid };
}

export default Atom;
