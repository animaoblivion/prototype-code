const isNotNull = (data) => {
  let value = data.value;
  value = value.trim();
  return (value === "" || value === null) ? "this field is required" : true;
}

export default isNotNull;
