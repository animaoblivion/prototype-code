const isNumber = (data) => {
  let value = data.value;
  value = value.trim();
  return (!isNaN(value)) ? true : "must be a number";
}

export default isNumber;
