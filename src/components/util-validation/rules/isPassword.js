const isPassword = (data) => {
  let value = data.value;
  value = value.trim();
  const pattern = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  return (!pattern.test(value)) ? true : "must not contain =!@#$%^&*()_+'";
}

export default isPassword;
