import isNotNull from "./isNotNull";
import isEmail from "./isEmail";
import isPassword from "./isPassword";
import isNumber from "./isNumber";
import isMin from "./isMin";
import isMax from "./isMax";
import isDate from "./isDate";
import isJson from "./isJson";

export default {
  isNotNull,
  isEmail,
  isPassword,
  isNumber,
  isMin,
  isMax,
  isDate,
  isJson
}
