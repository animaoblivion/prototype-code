const isMin = (data) => {
  let value = data.value;
  const config = data.config;
  const limit = config.max_limit;
  value = value.trim();
  return (value.length <= limit) ? true : "maximum of " + limit;
}

export default isMin;
