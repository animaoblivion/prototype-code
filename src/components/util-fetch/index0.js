import { useState, useEffect } from 'react';

const Atom = async (url, config) => {
  try {
    const response = await fetch(url, config);
    const data = await response.json();
    //if (response.status !== 200) throw data;
    if(!response.ok) throw data;
    return data;
  } catch(error) {
    return false;
  }
}

export default Atom;
