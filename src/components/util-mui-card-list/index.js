import Normal from './list-normal';
import Headless from './list-headless';

export default {
  Normal,
  Headless
}
