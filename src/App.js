import React from 'react';
import { Route, Switch } from "react-router-dom";

import Default from './layout/default';

const App = props => {
  return <Switch>
    <Route path="/" component={Default} />
  </Switch>
}

export default App;
